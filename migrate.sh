#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ] || [ ! -d $1 ] || [ ! -d $2 ]; then
    echo "Usage: bash migrate.sh [maven_repo] [gradle_repo]"
    echo "  e.g. bash migrate.sh  ~/.gradle/caches/modules-2/files-2.1  ~/.m2/repository"
    exit -1
fi

for GROUP_ID in `ls $1`
do
    SRC_GROUP=`echo $GROUP_ID | sed 's|\\.|/|g'`
    for ARTIFACT_ID in `ls $1/$GROUP_ID`
    do
        for VERSION in `ls $1/$GROUP_ID/$ARTIFACT_ID`
        do
            DEST=$2/$SRC_GROUP/$ARTIFACT_ID/$VERSION
            echo $DEST
            [[ -d $DEST ]] || mkdir -p $DEST
            for SHA1 in `ls $1/$GROUP_ID/$ARTIFACT_ID/$VERSION`
            do
                for FILE in `ls $1/$GROUP_ID/$ARTIFACT_ID/$VERSION/$SHA1`
                do
                    mv  $1/$GROUP_ID/$ARTIFACT_ID/$VERSION/$SHA1/$FILE  $DEST/
                    echo  $SHA1  >  $DEST/$FILE.sha1
                done
            done # SHA1
        done # VERSION
    done # ARTIFACT_ID
done # GROUP_ID
