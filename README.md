Usage
---
This is a bash script that move the POM and JAR files in the local Gradle repository to the local Maven repository, so that you don't need to store duplicated files (one for Gradle and one for Maven). You can use `mavenLocal()` in "gradle.build" to make use of the local Maven repository in Gradle.

```
bash  migrate.sh  [gradle_repo]  [mvn_repo]
```
e.g.
```
bash  migrate.sh  ~/.gradle/caches/modules-2/files-2.1  ~/.m2/repository
```

